﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoBusiness
    {
        public void CadastrarProduto(ProdutoDTO dto)
        {
            Regex regexValor = new Regex(@"^[0-9]{1,},[0-9]{2}$");
            Regex regexNm_Produto = new Regex(@"^[a-zA-Z0-9\sç]{0,100}$");

            if (regexNm_Produto.IsMatch(dto.nm_produto) == false)
                throw new ArgumentException("O nome do produto não deve conter acento ou caracteres especiais.");
            if (regexValor.IsMatch(dto.vl_preco.ToString()) == false)
                throw new ArgumentException("Você deve digitar um preço. Que deve estar no formato '0,00'");
            if (dto.nm_produto == string.Empty)
                throw new ArgumentException("Você deve digitar o nome do produto!");

            ProdutoDatabase db = new ProdutoDatabase();
            db.CadastrarProduto(dto);
        }

        public List<ProdutoDTO> ConsultarProdutos(ProdutoDTO dto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.ConsultarProdutos(dto);
        }
    }
}

                
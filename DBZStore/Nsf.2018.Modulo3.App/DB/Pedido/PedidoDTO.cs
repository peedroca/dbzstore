﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoDTO
    {
        public int id_pedido { get; set; }
        public string nm_cliente { get; set; }
        public string cpf { get; set; }
        public DateTime dt_venda { get; set; }
    }
}

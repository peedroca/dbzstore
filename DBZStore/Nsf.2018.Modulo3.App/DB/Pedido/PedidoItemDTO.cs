﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoItemDTO
    {
        public int id_pedido_item { get; set; }
        public int id_pedido { get; set; }
        public int id_produto { get; set; }
    }
}

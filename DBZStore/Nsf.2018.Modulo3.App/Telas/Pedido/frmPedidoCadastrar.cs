﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Pedido;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoCadastrar : UserControl
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();

        public frmPedidoCadastrar()
        {
            InitializeComponent();
            CarregarProdutos();
            ConfigurarGrid();
        }

        private void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }

        private void CarregarProdutos()
        {
            PedidoBusiness business = new PedidoBusiness();
            List<ProdutoDTO> produtos = business.ListarProdutos();

            cboProduto.ValueMember = nameof(ProdutoDTO.id_produto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.nm_produto);
            cboProduto.DataSource = produtos;
        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {
            try
            {
                PedidoDTO dto = new PedidoDTO();
                dto.nm_cliente = txtCliente.Text;
                dto.cpf = txtCpf.Text;
                dto.dt_venda = DateTime.Now;

                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto, produtosCarrinho.ToList());

                MessageBox.Show("Pedido emitido com sucesso!", "DBZ Store", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ Store", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro não identificado: " + ex.Message, "DBZ Store", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;

            if (txtQuantidade.Text != string.Empty)
            {
                for (int i = 0; i < int.Parse(txtQuantidade.Text); i++)
                {
                    produtosCarrinho.Add(dto);
                }
            }
            else
            {
                MessageBox.Show("Digite uma quantidade!", "DBZ Store", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRem_Click(object sender, EventArgs e)
        {
            if (produtosCarrinho.Count > 0)
            {
                int pos = dgvItens.CurrentRow.Index;
                produtosCarrinho.RemoveAt(pos);
            }
            else
            {
                MessageBox.Show("Não há itens a serem removidos!", "DBZ Store", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRemAll_Click(object sender, EventArgs e)
        {
            while(produtosCarrinho.Count > 0)
            {
                produtosCarrinho.RemoveAt(0);
            }
        }
    }
}

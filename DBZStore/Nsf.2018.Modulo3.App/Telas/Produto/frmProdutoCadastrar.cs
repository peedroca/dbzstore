﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoCadastrar : UserControl
    {
        public frmProdutoCadastrar()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPreco.Text.Contains(",") != true)
                    txtPreco.Text = $"{txtPreco.Text},00";

                ProdutoDTO dto = new ProdutoDTO();
                dto.nm_produto = txtProduto.Text;
                dto.vl_preco = txtPreco.Text != string.Empty ? Convert.ToDecimal(txtPreco.Text) : 0;

                ProdutoBusiness business = new ProdutoBusiness();
                business.CadastrarProduto(dto);

                MessageBox.Show("Produto cadastrado com sucesso!", "DBZ Store", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "DBZ Store", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro não identificado: " + ex.Message, "DBZ Store", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
